﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using haber_portal.Models;

namespace haber_portal.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            index model = new index();
            model = index.GetModel();
            return View("Index",model);
        }

        public ActionResult IndexByCategories(int kat,int? page,int? id)
        {
            if (id==null)
            {
                if (page==null)
                {
                    page = 1;
                }
                indexbycategories model = new indexbycategories();
                model = indexbycategories.GetModel(kat, (int)page);
                return View("IndexByCategories", model);
            }
            else
            {
                haberdetay model = new haberdetay();
                model = haberdetay.GetModel((int)id);
                return View("haberdetay", model);
            }
        }

        public ActionResult HaberDetay(int id)
        {
            haberdetay model = new haberdetay();
            model = haberdetay.GetModel(id);
            return View("haberdetay", model);
        }

        [HttpPost]
        public void uyekayit(string name, string password, string email, string return_url)
        {
            using (var kayit=new medyaseverEntities())
            {
                kayit.sp_ins_member(name, email, password, null, null, null);
            }
            Response.Redirect(return_url);
        }

        [HttpPost]
        public void uyegiris(string name2, string password2, string return_url)
        {
            using (var kayit = new medyaseverEntities())
            {
                var member = kayit.sp_get_member(name2, password2);
                foreach (var item in member)
                {
                    if (item.nick!=null)
                    {
                        Session["uname"] = item.nick;
                        Session["uid"] = item.id;
                    }
                }
                
            }
            Response.Redirect(return_url);
        }

        [HttpPost]
        public void yorum_yap(int uid, string yorumtext, int hid, string return_url)
        {
            using (var kayit = new medyaseverEntities())
            {
                kayit.sp_ins_yorum(uid, hid, yorumtext);
            }
            Response.Redirect(return_url);
        }
    }
}
