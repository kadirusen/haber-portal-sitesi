﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using haber_portal.Models;

namespace haber_portal.Controllers
{
    public class PanelController : Controller
    {
        //
        // GET: /Panel/

        public ActionResult Index()
        {
            Models.Panel.Haber model = new Models.Panel.Haber();
            model = Models.Panel.Haber.GetModel();
            return View("Index", model);
        }

        public ActionResult UyeOnay()
        {
            Models.Panel.Uye model = new Models.Panel.Uye();
            model = Models.Panel.Uye.GetModel();
            return View("UyeOnay", model);
        }

        public ActionResult YorumOnay()
        {
            Models.Panel.Uye model = new Models.Panel.Uye();
            model = Models.Panel.Uye.GetModel();
            return View("YorumOnay", model);
        }

        public ActionResult Moderasyon(int p,int durum,int id)
        {

            using (var onay2=new medyaseverEntities())
            {
                var onayla = onay2.sp_panel_onayla(p,durum,id);
            }
            Models.Panel.Uye model = new Models.Panel.Uye();
            model = Models.Panel.Uye.GetModel();
            if (p==0)
            {
                return View("Index", model);
            }
            else
            {
                if (p==1)
                {
                    return View("UyeOnay", model);
                }
                else
                {
                    return View("YorumOnay", model);
                }
            }
            
        }

        [HttpPost]
        public void HaberKaydet()
        {

        }
    }
}
