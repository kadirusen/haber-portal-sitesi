﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace haber_portal.Models
{
    public class indexbycategories
    {
        public List<all_news_by_cat> tum_haberler { get; set; }
        public string kategori { get; set; }

        public static indexbycategories GetModel(int id,int page)
        {
            indexbycategories model = new indexbycategories();

            using (var allnews_db = new Models.medyaseverEntities())
            {
                var allnews_list = allnews_db.sp_get_AllNews_By_Cat(id);
                string kategoriurl = "";
                var kategori = allnews_db.sp_get_cat_name(id);
                foreach (var item in kategori)
                {
                    model.kategori = item.kategori_adi;
                    kategoriurl = item.url;
                }
                
                var haberliste = new List<all_news_by_cat>();

                foreach (var item in allnews_list.Where(x => x.aktif == 1).Skip((page-1)*6))
                {
                    all_news_by_cat haber = new all_news_by_cat();

                    haber.haber_baslik = item.haber_baslik;
                    haber.haber_spot = item.haber_spot;
                    haber.haber_icerik = item.haber_icerik;
                    haber.haber_kucukresim = item.haber_kucukresim;
                    haber.haber_buyukresim = item.haber_buyukresim;
                    haber.haber_tarih = (DateTime)item.haber_tarih;
                    haber.kategori_adi = kategoriurl;
                    haber.haber_id = item.id;
                    haberliste.Add(haber);
                }
                model.tum_haberler = haberliste;
            

            }
        return model;
        }



            
    }


        public class all_news_by_cat
        {
            public string haber_baslik { get; set; }
            public string haber_spot { get; set; }
            public string haber_icerik { get; set; }
            public string haber_kucukresim { get; set; }
            public string haber_buyukresim { get; set; }
            public DateTime haber_tarih { get; set; }
            public int kategori_id { get; set; }
            public int haber_id { get; set; }
            public int aktif { get; set; }
            public string kategori_adi { get; set; }
        }

    }
