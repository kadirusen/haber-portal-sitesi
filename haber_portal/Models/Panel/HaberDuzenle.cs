﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace haber_portal.Models.Panel
{
    public class HaberDuzenle
    {
        public string kategori { get; set; }
        public string haber_baslik { get; set; }
        public string haber_spot { get; set; }
        public string haber_icerik { get; set; }
        public string haber_kucukresim { get; set; }
        public string haber_buyukresim { get; set; }
        public DateTime haber_tarih { get; set; }
        public int kategori_id { get; set; }
        public int aktif { get; set; }
        public string kategori_adi { get; set; }
        public int haber_id { get; set; }
        public List<all_cat_panel> tum_kategoriler_panel { get; set; }

        public static HaberDuzenle GetModel(int id)
        {
            HaberDuzenle model = new HaberDuzenle();

            using (var newsicerik_db = new Models.medyaseverEntities())
            {
                var news_icerik = newsicerik_db.sp_get_haberdetay(id);

                foreach (var item in news_icerik.Where(x => x.aktif == 1))
                {
                    model.haber_baslik = item.haber_baslik;
                    model.haber_spot = item.haber_spot;
                    model.haber_kucukresim = item.haber_kucukresim;
                    model.haber_buyukresim = item.haber_buyukresim;
                    model.haber_icerik = item.haber_icerik;
                    model.haber_tarih = (DateTime)item.haber_tarih;
                    model.kategori_adi = item.kategori_adi;
                    model.haber_id = item.id;
                    model.kategori_id = (int)item.kategori_id;
                }

                var kategori = newsicerik_db.sp_get_cat_name(model.kategori_id);
                foreach (var item in kategori)
                {
                    model.kategori = item.kategori_adi;
                }
                var kategoriliste = new List<all_cat_panel>();
                var kategori_list = newsicerik_db.sp_get_cat_name(null);
                foreach (var item2 in kategori_list)
                {
                    all_cat_panel kat = new all_cat_panel();
                    kat.kategori_id = (int)item2.id;
                    kat.kategori_adi = item2.kategori_adi;
                    kategoriliste.Add(kat);
                }
                model.tum_kategoriler_panel = kategoriliste;
            }
            return model;
        }
    }

    public class all_cat_panel
    {
        public int kategori_id { get; set; }
        public string kategori_adi { get; set; }
    }
}