﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace haber_portal.Models.Panel
{
    public class Uye:Controller
    {
        public List<uyeler_onay> uyeler_onay { get; set; }
        public List<yorumlar_onay> yorumlar_onay { get; set; }
        public List<haberler_onay> haberler_onay { get; set; }

        public static Uye GetModel()
        {
            Uye model = new Uye();

            using (var onay=new medyaseverEntities())
            {
                var yorum_onay = onay.sp_get_yorum();
                var yorum_onay_liste = new List<yorumlar_onay>();
                foreach (var item in yorum_onay)
                {
                    yorumlar_onay onay_liste = new yorumlar_onay();

                    onay_liste.uye_adi = item.nick;
                    onay_liste.yorum_icerik = item.yorum_icerik;
                    onay_liste.yorum_id = item.id;
                    onay_liste.yorum_tarih = (DateTime)item.yorum_tarih;
                    onay_liste.haber_baslik = item.haber_baslik;

                    yorum_onay_liste.Add(onay_liste);
                }
                model.yorumlar_onay = yorum_onay_liste;

                var uye_onay = onay.sp_get_members();
                var uye_onay_liste = new List<uyeler_onay>();
                foreach (var item in uye_onay)
                {
                    uyeler_onay onay_liste = new uyeler_onay();

                    onay_liste.nick = item.nick;
                    onay_liste.ad_soyad = item.adi+" "+item.soyadi;
                    onay_liste.sifre = item.sifre;
                    onay_liste.onay = item.aktif.Value;
                    onay_liste.eposta = item.email;
                    onay_liste.id = item.id;
                    uye_onay_liste.Add(onay_liste);
                }
                model.uyeler_onay = uye_onay_liste;

                var haber_onay = onay.sp_get_AllNews();
                var haber_onay_liste = new List<haberler_onay>();
                foreach (var item in haber_onay)
                {
                    haberler_onay onay_liste = new haberler_onay();

                    onay_liste.haber_baslik = item.haber_baslik;
                    onay_liste.haber_spot = item.haber_spot;
                    onay_liste.haber_icerik = item.haber_icerik;
                    onay_liste.haber_kucukresim = item.haber_kucukresim;
                    onay_liste.haber_tarih = (DateTime)item.haber_tarih;
                    onay_liste.kategori_adi = item.kategori_adi;
                    onay_liste.aktif = item.aktif.Value;
                    onay_liste.haber_id = item.id;
                    haber_onay_liste.Add(onay_liste);
                }
                model.haberler_onay = haber_onay_liste;
            }

            return model;
        }
    }

    public class uyeler_onay
    {
        public int id { get; set; }
        public string nick { get; set; }
        public string ad_soyad { get; set; }
        public string eposta { get; set; }
        public bool onay { get; set; }
        public string sifre { get; set; }
    }

    public class haberler_onay
    {
        public string haber_baslik { get; set; }
        public string haber_spot { get; set; }
        public string haber_icerik { get; set; }
        public string haber_kucukresim { get; set; }
        public string haber_buyukresim { get; set; }
        public DateTime haber_tarih { get; set; }
        public int kategori_id { get; set; }
        public int haber_id { get; set; }
        public int aktif { get; set; }
        public string kategori_adi { get; set; }
    }

    public class yorumlar_onay
    {
        public string haber_baslik { get; set; }
        public int yorum_id { get; set; }
        public int aktif { get; set; }
        public string uye_adi { get; set; }
        public string yorum_icerik { get; set; }
        public DateTime yorum_tarih { get; set; }
    }
}