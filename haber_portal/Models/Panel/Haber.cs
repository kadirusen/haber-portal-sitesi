﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace haber_portal.Models.Panel
{
    public class Haber
    {
        public List<all_cat_panel> tum_kategoriler_panel { get; set; }
        public List<all_news_panel> tum_haberler_panel { get; set; }


        public static Haber GetModel()
        {
            Haber model = new Haber();

            using (var allnews_db=new Models.medyaseverEntities())
            {
                var allnews_list = allnews_db.sp_get_AllNews();
                var indexslider_list=allnews_db.sp_get_indexslider();

                var haberliste = new List<all_news_panel>();


                foreach (var item in allnews_list)
                {
                    all_news_panel haber = new all_news_panel();
                    haber.aktif = (int)item.aktif;
                    haber.haber_baslik = item.haber_baslik;
                    haber.haber_spot = item.haber_spot;
                    haber.haber_icerik = item.haber_icerik;
                    haber.haber_kucukresim = item.haber_kucukresim;
                    haber.haber_buyukresim = item.haber_buyukresim;
                    haber.haber_tarih = (DateTime)item.haber_tarih;
                    haber.kategori_adi = item.kategori_adi;
                    haber.kategori_id = (int)item.kategori_id;
                    var kategori = allnews_db.sp_get_cat_name((int)item.kategori_id);
                    foreach (var item2 in kategori)
                    {
                        haber.kategori_url = item2.url;
                    }
                    haber.haber_id = (int)item.id;
                    haberliste.Add(haber);
                }
                model.tum_haberler_panel = haberliste;


            }

            return model;
        }
    }
    public class all_news_panel
    {
        public string kategori_url { get; set; }
        public int haber_id { get; set; }
        public string haber_baslik { get; set; }
        public string haber_spot { get; set; }
        public string haber_icerik { get; set; }
        public string haber_kucukresim { get; set; }
        public string haber_buyukresim { get; set; }
        public DateTime haber_tarih { get; set; }
        public int kategori_id { get; set; }
        public int aktif { get; set; }
        public string kategori_adi { get; set; }
    }
}