﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace haber_portal.Models
{
    public class haberdetay : Controller 
    {
        public string kategori { get; set; }
        public string haber_baslik { get; set; }
        public string haber_spot { get; set; }
        public string haber_icerik { get; set; }
        public string haber_kucukresim { get; set; }
        public string haber_buyukresim { get; set; }
        public DateTime haber_tarih { get; set; }
        public int kategori_id { get; set; }
        public int aktif { get; set; }
        public string kategori_adi { get; set; }
        public string kategori_url { get; set; }
        public int haber_id { get; set; }
        public List<all_news_by_cat> ilgili_5_haber { get; set; }
        public List<yorumlar> yorumlar { get; set; }

        public static haberdetay GetModel(int id)
        { 
         haberdetay model=new haberdetay();

         using (var newsicerik_db = new Models.medyaseverEntities())
         {
             var news_icerik = newsicerik_db.sp_get_haberdetay(id);

             foreach (var item in news_icerik.Where(x=> x.aktif==1))
	{
                   model.haber_baslik = item.haber_baslik;
                   model.haber_spot = item.haber_spot;
                   model.haber_kucukresim = item.haber_kucukresim;
                   model.haber_buyukresim = item.haber_buyukresim;
                   model.haber_icerik = item.haber_icerik;
                   model.haber_tarih = (DateTime)item.haber_tarih;
                   model.kategori_adi = item.kategori_adi;
                   model.haber_id = item.id;
                   model.kategori_id = (int)item.kategori_id;
    }

             var kategori = newsicerik_db.sp_get_cat_name(model.kategori_id);
             foreach (var item in kategori)
             {
                 model.kategori = item.kategori_adi;
                 model.kategori_url = item.url;
             }

             var haberliste = new List<all_news_by_cat>();

             var allnews_list = newsicerik_db.sp_get_AllNews_By_Cat(model.kategori_id);
             foreach (var item in allnews_list.Where(x => x.aktif == 1).OrderByDescending(z=>z.haber_tarih).Take(10))
             {
                 all_news_by_cat haber = new all_news_by_cat();

                 haber.haber_baslik = item.haber_baslik;
                 haber.haber_spot = item.haber_spot;
                 haber.haber_kucukresim = item.haber_kucukresim;
                 haber.haber_tarih = (DateTime)item.haber_tarih;
                 haber.haber_id = item.id;
                 haberliste.Add(haber);
             }
             model.ilgili_5_haber = haberliste;

             var yorumliste = new List<yorumlar>();

             var yorumliste_list = newsicerik_db.sp_get_yorum_by_newsid(model.haber_id);
             foreach (var item in yorumliste_list.OrderByDescending(z => z.yorum_tarih).Take(10))
             {
                 yorumlar haber = new yorumlar();

                 haber.nick = item.nick;
                 haber.yorum_icerik = item.yorum_icerik;
                 haber.yorum_tarih = (DateTime)item.yorum_tarih;
                 yorumliste.Add(haber);
             }
             model.yorumlar = yorumliste;

                      }
         return model;
        }
   }
    public class yorumlar
    {
        public string nick { get; set; }
        public string yorum_icerik { get; set; }
        public DateTime yorum_tarih { get; set; }
    }
}
