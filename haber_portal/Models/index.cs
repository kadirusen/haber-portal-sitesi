﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace haber_portal.Models
{
   
    public class index
    {
        public List<manset> mansetler {get;set;}
        public List<all_news> tum_haberler { get; set; }

        public static index GetModel()
        {
            index model = new index();

            using (var allnews_db=new Models.medyaseverEntities())
            {
                var allnews_list = allnews_db.sp_get_AllNews();
                var indexslider_list=allnews_db.sp_get_indexslider();

                var haberliste = new List<all_news>();
                var mansetliste = new List<manset>();

                foreach (var item in allnews_list.Where(x=>x.aktif==1))
                {
                    all_news haber = new all_news();
                    haber.haber_baslik = item.haber_baslik;
                    haber.haber_spot = item.haber_spot;
                    haber.haber_icerik = item.haber_icerik;
                    haber.haber_kucukresim = item.haber_kucukresim;
                    haber.haber_buyukresim = item.haber_buyukresim;
                    haber.haber_tarih = (DateTime)item.haber_tarih;
                    haber.kategori_adi = item.kategori_adi;
                    haber.kategori_id = (int)item.kategori_id;
                    var kategori = allnews_db.sp_get_cat_name((int)item.kategori_id);
                    foreach (var item2 in kategori)
                    {
                        haber.kategori_url = item2.url;
                    }
                    haber.haber_id = (int)item.id;
                    haberliste.Add(haber);
                }
                model.tum_haberler = haberliste;

                foreach (var item in indexslider_list.Where(y=>y.aktif==1))
                {
                    manset manset = new manset();
                    manset.title = item.haber_baslik;
                    manset.short_text = item.haber_spot;
                    manset.image = item.haber_buyukresim;
                    manset.publish_date = (DateTime)item.haber_tarih;
                    mansetliste.Add(manset);

          
                   

                }
                model.mansetler = mansetliste;
            }


              


            return model;
        }
    }


    public class manset
    {
        public string title{get;set;}
        public string image{get;set;}
        public DateTime publish_date{get;set;}
        public string short_text{get;set;}
    }

 public class all_news
{
     public string kategori_url { get; set; }
        public int haber_id { get; set; }
        public string haber_baslik{get;set;}
        public string haber_spot{get;set;}
        public string haber_icerik { get; set; }
        public string haber_kucukresim { get; set; }
        public string haber_buyukresim { get; set; }
        public DateTime haber_tarih{get;set;}
        public int kategori_id{get;set;}
        public int aktif{get;set;}
        public string kategori_adi { get; set; }
}

}