﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace haber_portal
{
    public class RouteConfig
    {


        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                   name: "SporHaberleri",
                   url: "spor_haberleri/{id}",
                   defaults: new { controller = "Home", action = "IndexByCategories", id = UrlParameter.Optional, kat = 1 }
               );

            routes.MapRoute(
        name: "SporHaberleriPage",
        url: "spor_haberleri/sayfa/{page}",
        defaults: new { controller = "Home", action = "IndexByCategories", page = 1,kat=1 }
    );
            routes.MapRoute(
       name: "SaglikTeknoloji",
       url: "saglik_ve_teknoloji/{id}",
       defaults: new { controller = "Home", action = "IndexByCategories", id = UrlParameter.Optional, kat=2 }
   );
            routes.MapRoute(
                   name: "SaglikTeknolojiPage",
                   url: "saglik_ve_teknoloji/sayfa/{page}",
                   defaults: new { controller = "Home", action = "IndexByCategories", page = 1, kat = 2 }
               );
            routes.MapRoute(
       name: "EgitimHaberleri",
       url: "egitim_haberleri/{id}",
       defaults: new { controller = "Home", action = "IndexByCategories", id = UrlParameter.Optional, kat = 3 }
   );

            routes.MapRoute(
        name: "EgitimHaberleriPage",
        url: "egitim_haberleri/sayfa/{page}",
        defaults: new { controller = "Home", action = "IndexByCategories", page = 1, kat = 3 }
    );
            routes.MapRoute(
       name: "magazin",
       url: "magazin_haberleri/{id}",
       defaults: new { controller = "Home", action = "IndexByCategories", id = UrlParameter.Optional, kat = 4 }
   );
            routes.MapRoute(
                   name: "magazinPage",
                   url: "magazin_haberleri/sayfa/{page}",
                   defaults: new { controller = "Home", action = "IndexByCategories", page = 1, kat = 4 }
               );
            routes.MapRoute(
       name: "FinansHaberleri",
       url: "finan_haberleri/{id}",
       defaults: new { controller = "Home", action = "IndexByCategories", id = UrlParameter.Optional, kat = 5 }
   );

            routes.MapRoute(
        name: "FinansHaberleriPage",
        url: "finan_haberleri/sayfa/{page}",
        defaults: new { controller = "Home", action = "IndexByCategories", page = 1, kat = 5 }
    );
            routes.MapRoute(
       name: "dunya",
       url: "dunya_haberleri/{id}",
       defaults: new { controller = "Home", action = "IndexByCategories", id = UrlParameter.Optional, kat = 6 }
   );
            routes.MapRoute(
                   name: "dunyaPage",
                   url: "dunya_haberleri/sayfa/{page}",
                   defaults: new { controller = "Home", action = "IndexByCategories", page = 1, kat = 6 }
               );

            routes.MapRoute(
       name: "Mods",
       url: "panel/moderasyon/{p}/{durum}/{id}",
       defaults: new { controller = "Panel", action = "Moderasyon"}
   );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

         



        }
    }
}